アライさん bot (偽)
====================

本家のアライさん bot がお隠れになってしまったので、戻ってくるまでのつなぎとして作ったのだ。

タヌキじゃないですのだ。

なんとなくオープンソースにしてあるのでマージリクエストお待ちしていますのだ!

実装機能
--------------------

* 「～いい?」と聞くと可決か否決か返してくれる機能(ちほーMAGI)
    * 「アライさん、どれがいい?」「select」などに続けて改行区切りの選択肢を与えるとひとつ選んでくれる機能
* 同じがおーの連続をカウントしてくれる機能(けもコーラス)

本家にあったけど未実装な機能
--------------------

* あらーむ全般
    * アラーム
    * ご飯をあげてレベルアップしたりする機能
        * 死ぬ機能
* けもコーラスなどを交えつつけものフレンズ 3 風の戦闘をする機能
* `system shutdown` で再起動する機能
* ㄘんㄘんを切断する機能

他にもあった気もするけど覚えていないのだ!　情報求みますのだ。

依存情報
--------------------

この bot は Python で作られているのだ。ライブラリの依存関係については `requirements.txt` に記載してあるので、

```shell
pip install -r requirements.txt
```

でインストールできるのだ。

なお、Windows 版の場合はこれに加えて

```shell
pip install python-magic-bin
```

もしておく必要があるのだ。

免責事項
--------------------

本 bot を利用して生じた不利益について、申し訳ないけど知ったこっちゃないのだ。でもできる限りのことはするのだ。

文句があったら[こいつ](https://mstdn.kemono-friends.info/@BlueRayi)に言うのだ。