from __future__ import annotations

from pathlib import Path
from typing import Any

class StreamListener: ...

class Mastodon:
    def __init__(
        self, api_base_url: str, access_token: str | Path
    ) -> None: ...
    def toot(self, status: str) -> None: ...
    def stream_local(
        self,
        listener: StreamListener,
        run_async: bool = ...,
        timeout: int = ...,
        reconnect_async: bool = ...,
        reconnect_async_wait_sec: int = ...,
    ) -> None: ...

class AttribAccessDict(dict[str, Any]): ...
