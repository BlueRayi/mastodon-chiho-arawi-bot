from __future__ import annotations

import random
from dataclasses import dataclass
from pathlib import Path
from re import Pattern, compile
from typing import ClassVar, TypeVar
from unicodedata import normalize

import pendulum
from html2text import HTML2Text
from mastodon import AttribAccessDict, Mastodon, StreamListener
from pendulum.datetime import DateTime

from return_dict import TootDict, UserDict

T = TypeVar("T")

INSTANCE_URL = "https://mstdn.kemono-friends.info/"
NORMALIZE_FORM = "NFKD"


@dataclass(frozen=True)
class Choice:
    order: int
    detail: str

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Choice):
            return False
        return self.detail == other.detail

    def __hash__(self) -> int:
        return self.detail.__hash__()

    def __str__(self) -> str:
        return self.detail

    @property
    def one_based_order(self) -> int:
        return self.order + 1

    @property
    def japanese_order(self) -> str:
        counter_word = "つ" if self.one_based_order < 10 else "個"
        return f"{self.one_based_order}{counter_word}目"


class AraiListener(StreamListener):
    _sender_token: Path
    _last_gaoh: str
    _chorus_users: list[UserDict]

    html_plainizer: ClassVar[HTML2Text]
    yesno_regex: ClassVar[Pattern[str]]
    standard_choose_regex: ClassVar[Pattern[str]]
    choose_regex: ClassVar[Pattern[str]]

    html_plainizer = HTML2Text()
    html_plainizer.ignore_links = True
    yesno_regex = compile(normalize(NORMALIZE_FORM, R"(い|好|良|善)い\?$"))
    standard_choose_regex = compile(
        normalize(NORMALIZE_FORM, R"^select\s*([\r\n]|$)")
    )
    choose_regex = compile(
        normalize(
            NORMALIZE_FORM,
            (
                R"^(ア|あ)(ラ|ら)((イ|い)|(ヰ|ゐ))さん(、|,)\s*"
                R"(どっち|どちら|どれ|誰|だれ)が(い|好|良|善)い\?\s*([\r\n]|$)"
            ),
        )
    )

    def __init__(self, sender_token: Path) -> None:
        self._sender_token = sender_token
        self._last_gaoh = ""
        self._chorus_users = []

    def on_update(self, response: AttribAccessDict) -> None:
        status = TootDict.from_response(response)
        self.yesno_process(status)
        self.choose_process(status)
        self.chorus_process(status)

    def on_abort(self, err: Exception) -> None:
        print(err)

    def yesno_process(self, status: TootDict) -> None:
        normalized_content = self.take_normalized_content(status)
        if self.is_yesno(normalized_content) and not self.is_choose(
            normalized_content
        ):
            iso_now = get_iso_now()
            linefeed_replaced_content = linefeed_replace(normalized_content)
            print(f"[{iso_now}] 是非疑問文")
            print(f"[{iso_now}] 発言者: @{status.account.acct}")
            print(f"[{iso_now}] 発言: {linefeed_replaced_content}")
            response = self.yesno_result()
            Mastodon(
                api_base_url=INSTANCE_URL, access_token=self._sender_token
            ).toot(response)
            iso_now = get_iso_now()
            linefeed_replaced_responce = linefeed_replace(response)
            print(f"[{iso_now}] 返答: {linefeed_replaced_responce}")

    def choose_process(self, status: TootDict) -> None:
        normalized_content = self.take_normalized_content(status)
        if self.is_choose(normalized_content):
            iso_now = get_iso_now()
            linefeed_replaced_content = linefeed_replace(normalized_content)
            print(f"[{iso_now}] 選択疑問文")
            print(f"[{iso_now}] 発言者: @{status.account.acct}")
            print(f"[{iso_now}] 発言: {linefeed_replaced_content}")
            choices = self.choices_of(normalized_content)
            response = self.choose_result(choices)
            Mastodon(
                api_base_url=INSTANCE_URL, access_token=self._sender_token
            ).toot(response)
            iso_now = get_iso_now()
            linefeed_replaced_responce = linefeed_replace(response)
            print(f"[{iso_now}] 返答: {linefeed_replaced_responce}")

    def chorus_process(self, status: TootDict) -> None:
        user = status.account
        normalized_content = self.take_normalized_content(status)
        chorus_count = len(self._chorus_users)
        chorus_is_ongoing = (
            normalized_content == self._last_gaoh
            and normalized_content != ""
            and user not in self._chorus_users
        )
        if chorus_is_ongoing:
            self._chorus_users.append(user)
        else:
            if chorus_count > 1:
                iso_now = get_iso_now()
                participants_str = ", ".join(
                    f"@{chorus_user.acct}"
                    for chorus_user in self._chorus_users
                )
                print(f"[{iso_now}] コーラス終了")
                print(f"[{iso_now}] 参加人数: {chorus_count}")
                print(f"[{iso_now}] 参加者: {participants_str}")
                print(f"[{iso_now}] 内容: {linefeed_replace(self._last_gaoh)}")
                postfix = "♪" if chorus_count > 3 else ""
                Mastodon(
                    api_base_url=INSTANCE_URL, access_token=self._sender_token
                ).toot(f"{chorus_count}コーラスなのだ{postfix}")
            self._last_gaoh = normalized_content
            self._chorus_users = [user]

    @classmethod
    def take_normalized_content(cls, status: TootDict) -> str:
        plainized_content = cls.take_plainized_content(status)
        return normalize(NORMALIZE_FORM, plainized_content)

    @classmethod
    def take_plainized_content(cls, status: TootDict) -> str:
        return cls.plainize_html(status.content).strip()

    @classmethod
    def plainize_html(cls, html: str) -> str:
        return cls.html_plainizer.handle(html)

    @classmethod
    def choices_of(cls, gaoh: str) -> set[Choice]:
        return {
            Choice(*indexed_line)
            for indexed_line in enumerate(
                line.strip() for line in gaoh.splitlines()[1:]
            )
        }

    @classmethod
    def is_yesno(cls, gaoh: str) -> bool:
        return cls.yesno_regex.search(gaoh) is not None

    @classmethod
    def is_choose(cls, gaoh: str) -> bool:
        return (
            cls.standard_choose_regex.search(gaoh) is not None
            or cls.choose_regex.search(gaoh) is not None
        )

    @classmethod
    def yesno_result(cls) -> str:
        return "いいのだ" if random.randint(0, 1) else "ダメなのだ"

    @classmethod
    def choose_result(cls, choices: set[Choice]) -> str:
        if len(choices) == 0:
            return "…? :nasa:​を取得したのだ"
        elif len(choices) == 1:
            return f"選択の余地がないのだ! {cls.choice_random(choices)} しかないのだ!"
        else:
            choice = cls.choice_random(choices)
            return f"{choice.japanese_order}の {choice} がいいのだ"

    @classmethod
    def choice_random(cls, choices_set: set[T]) -> T:
        return random.choice(list(choices_set))


def get_iso_now() -> str:
    return get_now().isoformat(timespec="seconds")


def get_now() -> DateTime:
    return pendulum.now("Asia/Tokyo")


def linefeed_replace(s: str) -> str:
    lines = s.splitlines()
    lines = [line[:-2] for line in lines[:-1]] + lines[~0:]
    return "\033[7m^J\033[0m".join(lines)


def main() -> None:
    pwd = Path()
    receiver_token = pwd / "receiver_token.txt"
    sender_token = pwd / "sender_token.txt"
    listener = AraiListener(sender_token)
    receiver = Mastodon(api_base_url=INSTANCE_URL, access_token=receiver_token)
    receiver.stream_local(listener, run_async=True, reconnect_async=True)
    Mastodon(api_base_url=INSTANCE_URL, access_token=sender_token).toot(
        "起動したのだ!"
    )
    c = ""
    while c != "exit":
        c = input()
    Mastodon(api_base_url=INSTANCE_URL, access_token=sender_token).toot(
        "さっ 寝るのだ\n(手動停止)"
    )


if __name__ == "__main__":
    main()
