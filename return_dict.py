from __future__ import annotations

from dataclasses import dataclass

from mastodon import AttribAccessDict


@dataclass(frozen=True)
class TootDict:
    account: UserDict
    content: str
    visibility: str

    @classmethod
    def from_response(cls, d: AttribAccessDict) -> TootDict:
        account = UserDict.from_response(d["account"])
        content = d["content"]
        visibility = d["visibility"]
        return TootDict(account, content, visibility)


@dataclass(frozen=True)
class UserDict:
    id: int
    acct: str

    @classmethod
    def from_response(cls, d: AttribAccessDict) -> UserDict:
        id = d["id"]
        acct = d["acct"]
        return UserDict(id, acct)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, UserDict):
            return False
        return self.id == other.id

    def __hash__(self) -> int:
        return self.id
